
def get_city_and_when(list_words):
    #print(list_words)
    for idx in range(len(list_words)):
        list_words[idx] = list_words[idx].lower()

    print(list_words)
    # Moscow
    if len(list_words) == 1:
        city = list_words[0]
        when = 'Now'
        return city, when
    # Moscow now
    if len(list_words) == 2 and list_words[1] == 'now':
        city = list_words[0]
        when = 'Now'
        return city, when

    # погода в Moscow
    if len(list_words) == 3 and list_words[0] == 'погода' and list_words[1] == 'в':
        city = list_words[2]
        when = 'Now'
        return city, when

    # какая погода в Moscow
    if len(list_words) == 4 and list_words[0] == 'какая' and list_words[1] == 'погода' \
            and list_words[2] == 'в':
        city = list_words[3]
        when = 'Future'
        return city, when

    # Moscow в понедельник
    if len(list_words) == 3 and list_words[1] == 'в':
        city = list_words[0]
        when = 'Future'
        return city, when

    # Moscow на неделе
    if len(list_words) == 3 and list_words[1] == 'на' and list_words[2] == 'неделе':
        city = list_words[0]
        when = 'Future'
        return city, when
    # Moscow in the week
    if len(list_words) == 4 and list_words[1] == 'in' and list_words[2] == 'the' and list_words[3] == 'week':
        city = list_words[0]
        when = 'Future'
        return city, when
    return None, None