import constats
import requests
import random
import urllib.request as urllib2

def get_img_url(city, when):
    search_url = "https://api.cognitive.microsoft.com/bing/v7.0/images/search"
    search_term = 'пейзаж города ' + city

    headers = {"Ocp-Apim-Subscription-Key": constats.subscription_key}
    params = {"q": search_term, "license": "public", "imageType": "photo", 'safeSearch' : 'Moderate'}
    response = requests.get(search_url, headers=headers, params=params)
    response.raise_for_status()
    search_results = response.json()

    thumbnail_urls = [img["thumbnailUrl"] for img in search_results["value"][:16]]
    print('urls imeges = ', thumbnail_urls)
    if (len(thumbnail_urls) == 0):
        return None
    if len(thumbnail_urls) > 20:
        thumbnail_urls = thumbnail_urls[:20]
    return random.choice(thumbnail_urls)

def send_photo_on_url(bot, message, url_img):
    if url_img is None:
        bot.send_message(message.chat.id, "Я картинки не нашеел :(")
    else:
        urllib2.urlretrieve(url_img, 'url_img.jpg')
        img = open('url_img.jpg', 'rb')
        bot.send_photo(message.from_user.id, img)
        img.close()