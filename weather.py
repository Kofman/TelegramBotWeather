from constats import appid
import requests

def check_city(s_city):
    city_id = -1
    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/find",
                            params={'q': s_city, 'type': 'like', 'units': 'metric', 'APPID': appid})
        data = res.json()
        cities = ["{} ({})".format( d['name'], d['sys']['country'] )
                  for d in data['list']]
        print("city:", cities)
        city_id = data['list'][0]['id']
        print('city_id=', city_id)
    except Exception as e:
        print("Exception (find):", e)
        return -1
    return city_id

def get_weather_now(city_id):
    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/weather",
                            params={'id': city_id, 'units': 'metric', 'lang': 'ru', 'APPID': appid})
        data = res.json()
        print('request weather is ok')

        result = "Погодка:" + str(data['weather'][0]['description']) + '\n' + \
         "температура:\t" + str(data['main']['temp']) + '\n' + \
         "температура минимальная:\t" + str(data['main']['temp_min']) + '\n' + \
         "температура максимальная:\t" + str(data['main']['temp_max']) + '\n'
        print(result)
    except Exception as e:
        print("Exception (weather):", e)
        return 'I have problem with weather'
    return result


def get_weather_future(city_id):
    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/forecast",
                            params={'id': city_id, 'units': 'metric', 'lang': 'ru', 'APPID': appid})
        data = res.json()
        result = ''
        print('len of list weather = ', len(data['list']))
        for i in data['list'][:9]:
            result += str(i['dt_txt']) + ' ' + '{0:+3.0f}'.format(i['main']['temp']) + ' ' + str(i['weather'][0]['description']) + '\n\n'
            print(i['dt_txt'], '{0:+3.0f}'.format( i['main']['temp'] ), i['weather'][0]['description'])
    except Exception as e:
        print("Exception (forecast):", e)
        return 'I have problem with weather'
    return result

def get_weather(city, when):
    city_id = check_city(city)
    if city_id == -1:
        return 'I have problem with city'

    if when == 'Now':
        return get_weather_now(city_id)

    if when == 'Future':
        return get_weather_future(city_id)
    return str(city_id)