# -*- coding: utf-8 -*-

import telebot
import constats



from weather import get_weather
from photo import get_img_url
from photo import send_photo_on_url
from parser import get_city_and_when

def main():
    bot = telebot.TeleBot(constats.token_bot)

    @bot.message_handler(commands=['help'])
    def handle_text(message):
        bot.send_message(message.chat.id, constats.help_msg)

    @bot.message_handler( commands=['start'] )
    def handle_text(message):
        bot.send_message(message.chat.id, constats.start_msg)

    @bot.message_handler(content_types=['text'])
    def handle_text(message):
        request = message.text
        list_words = request.split()

        city, when = get_city_and_when(list_words)
        print(city, when)

        if city is None and when is None:
            bot.send_message(message.chat.id, "Я тебя не понял :(")
            return
        else:
            weather = get_weather(city, when)
            bot.send_message(message.chat.id, weather)

            url_img = get_img_url(city, when)
            send_photo_on_url(bot, message, url_img)

        bot.send_message(message.chat.id, "Проси погоду!")


    bot.polling(none_stop=True, interval=0)

if __name__ == "__main__":
    main()